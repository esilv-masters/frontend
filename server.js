var express = require('express');
var path = require('path');
var mongo = require('mongoose');

const MONGODB_URI = 'mongodb+srv://ominga:5Oq9806sB73LViW1@mongodb-forumasso-c308ff20.mongo.ondigitalocean.com/forumasso_discordjs?authSource=admin&replicaSet=mongodb-forumasso&tls=true&tlsCAFile=ca-certificate.crt';

var db = mongo.connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }).then(connection => {
    console.log(`Connected to MongoDB`)
  }).catch(err => {
    if (err) throw err;
  });
  
var app = express()
app.use(express.json({limit:'5mb'}));
app.use(express.urlencoded({
  extended: true
}));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-Woth,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var Schema = mongo.Schema;

const AssoSchema = new Schema({
    assoName: {
        type: String,
        required: true,
        unique: true
    },
    userStatus: String,
    userIsBan: Boolean,
});


const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    discordId: {
        type: String,
        required: true,
        unique: true
    },
    username: {
        type: String,
        required: true
    },
    avatarURL: {
        type: String,
        required: true
    },
    isMod: {
        type: Boolean,
        default: false
    },
    isSpMod: {
        type: Boolean,
        default: false
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isReferent: {
        type: Boolean,
        default: false
    },
    school: String,
    schoolYear: Number,
    assosAsStaff: [AssoSchema],
    assosAsVisitor: [AssoSchema],
});

var model = mongo.model('users', UserSchema, 'users');

app.get('/api/users', (req, res) => {
    model.find({}, (err, data) => {
        if (err){
            res.send(err);
        } else {
            res.send(data);
        }
    });
});

app.listen(8080);