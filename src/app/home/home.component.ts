import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { HttpService } from '../services/http.service';
import { Router } from '@angular/router';
import { User } from '../models/user';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  authUser: User;

  constructor(
    public userService: UserService,
    private router: Router,

  ) { }

  ngOnInit(): void {
    if (this.userService.isCompleteProfile()) {
      // redirect to visitor or staff page
      if (this.userService.isModo()) {
        // redirect to moderator panel
        this.router.navigate(['/moderator']);
      } else if (this.userService.isStaff()) { 
        // redirect to staff panel
        this.router.navigate(['/staff']);
      }
    } else {
      // redirect to form
      this.router.navigate(['/infos']);
    }

    this.authUser = this.userService.currUser;

  }

  navigateToDiscord(): void {
    window.open('https://discord.gg/X77ztWs', '_blank');
  }

}
