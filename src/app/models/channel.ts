export interface Channel {
    id: string;
    emoji: string;
    name: string;
    fullname: string;
    type: "text" | "voice";
    permissionOverwrites: Permission[];
}

export interface Permission {
    id: "visitorRoleId";
    allow: PermId[];
    deny: PermId[];
}

export enum PermId {
    view = "VIEW_CHANNEL",
    write = "SEND_MESSAGES",
    connect = "CONNECT",
    speak = "SPEAK"
}