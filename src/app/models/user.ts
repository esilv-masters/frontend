export interface User {
    firstName?: string;
    lastName?: string;
    school?: string;
    schoolYear?: number;

    discordId: string;
    username: string;
    avatarURL: string;
    isMod: boolean;
    isSpMod: boolean;
    isAdmin: boolean;
    isReferent: boolean;

    assosAsStaff: UserAssociation[];
    assosAsVisitor: UserAssociation[];
}

export interface UserAssociation {
    assoName: string;
    userStatus?: 'pending' | 'accepted' | 'refused';
    userIsBan?: boolean;
}
