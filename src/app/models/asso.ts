export interface Asso {
    _id: string;
    linkedRoleId: string;
    name: string;
    emoji?: string;
    color?: string;
    category?: string;
    referentId?: string;
    referentName?: string;
}
