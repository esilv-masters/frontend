import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { User } from '../models/user';
import { HttpService } from '../services/http.service';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-form-infos',
  templateUrl: './form-infos.component.html',
  styleUrls: ['./form-infos.component.scss']
})
export class FormInfosComponent implements OnInit {

  isStaff = false;

  //------------AUTOCOMPLETE--------------//
  selectable = true;
  removable = true;
  filteredAssosAsVisitor: Observable<string[]> | undefined;
  assosAsVisitor: string[] = [];
  filteredAssosAsStaff: Observable<string[]> | undefined;
  assosAsStaff: string[] = [];
  allAssos: string[];

  @ViewChild('assoVisitorInput') assoVisitorInput!: ElementRef<HTMLInputElement>;
  @ViewChild('assoStaffInput') assoStaffInput!: ElementRef<HTMLInputElement>;
  //-------------------------------------//

  infosForm = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    ecole: new FormControl('', Validators.required),
    annee: new FormControl(null, Validators.required),
    assoAsVisitor: new FormControl(''),
    assoAsStaff: new FormControl('')
  });

  ecoles = ['EMLV', 'ESILV', 'IIM'];
  annees = [1, 2, 3, 4, 5];

  constructor(
    private userService: UserService,
    private http: HttpService,
    private store: StorageService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    if (this.userService.isCompleteProfile()) {
      this.router.navigate(['']);
    }

    this.http.getAssos().subscribe(data => {
      this.store.assos = data;

      this.allAssos = data.map(asso => asso.name);
      this.filteredAssosAsVisitor = this.assoAsVisitor?.valueChanges.pipe(
        startWith(null),
        map((asso: string | null) => asso ? this._filter(asso) : this.allAssos?.slice()));
  
      this.filteredAssosAsStaff = this.assoAsStaff?.valueChanges.pipe(
        startWith(null),
        map((asso: string | null) => asso ? this._filter(asso) : this.allAssos?.slice()));
    })
  }

  onValidate(): void {
    if (this.infosForm.valid) {
      const infos = {
        firstName: this.prenom?.value,
        lastName: this.nom?.value,
        school: this.ecole?.value,
        schoolYear: this.annee?.value,
        assosAsVisitor: this.assosAsVisitor,
        assosAsStaff: this.assosAsStaff
      }
      this.http.updateUserInfos(this.userService.discordId, infos).subscribe((data: User) => {
        this.userService.currUser = data;
        this.router.navigate(['']);
      });
    }
  }

  get nom() { return this.infosForm.get('nom'); }
  get prenom() { return this.infosForm.get('prenom'); }
  get ecole() { return this.infosForm.get('ecole'); }
  get annee() { return this.infosForm.get('annee'); }
  get assoAsVisitor() { return this.infosForm.get('assoAsVisitor'); }
  get assoAsStaff() { return this.infosForm.get('assoAsStaff'); }

    //-------------------//

    addVisitor(event: MatChipInputEvent): void {
      const value = (event.value || '').trim();
      if (value) {
        this.assosAsVisitor.push(value);
      }
      // Clear the input value
      event.chipInput!.clear();
      this.assoAsVisitor?.setValue(null);
    }
  
    removeVisitor(asso: string): void {
      const index = this.assosAsVisitor.indexOf(asso);
      if (index >= 0) {
        this.assosAsVisitor.splice(index, 1);
      }
    }
  
    selectedVisitor(event: MatAutocompleteSelectedEvent): void {
      this.assosAsVisitor.push(event.option.viewValue);
      this.assoVisitorInput.nativeElement.value = '';
      this.assoAsVisitor?.setValue(null);
    }

    addStaff(event: MatChipInputEvent): void {
      const value = (event.value || '').trim();
      if (value) {
        this.assosAsStaff.push(value);
      }
      event.chipInput!.clear();
      this.assoAsStaff?.setValue(null);
    }
  
    removeStaff(fruit: string): void {
      const index = this.assosAsStaff.indexOf(fruit);
      if (index >= 0) {
        this.assosAsStaff.splice(index, 1);
      }
    }
  
    selectedStaff(event: MatAutocompleteSelectedEvent): void {
      this.assosAsStaff.push(event.option.viewValue);
      this.assoStaffInput.nativeElement.value = '';
      this.assoAsStaff?.setValue(null);
    }
  
    private _filter(value: string): string[] {
      const filterValue = value.toLowerCase();  
      return this.allAssos.filter(asso => asso.toLowerCase().includes(filterValue));
    }

}
