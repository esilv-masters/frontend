import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { AuthService } from '@auth0/auth0-angular';
import { environment as env } from '../../environments/environment';

import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';
import { Asso } from '../models/asso';
import { Channel } from '../models/channel';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  apiURL = "https://api.forum-associatif-numerique.fr/api";

  constructor(
    private http: HttpClient,
    public auth: AuthService,
  ) { }

  public getAssos(): Observable<Asso[]> { 
    return this.http.get<Asso[]>(`${this.apiURL}/assos`);
  }

  public getUsers(): Observable<User[]> { 
    return this.http.get<User[]>(`${this.apiURL}/users`);
  }

  public getUserByDiscordId(discordId: string): Observable<User> { 
    return this.http.get<User>(`${this.apiURL}/users/${discordId}`);
  }

  public updateUserInfos(discordId: string, infos: Object): Observable<User> { 
    return this.http.patch<User>(`${this.apiURL}/users/${discordId}`, infos);
  }

  public updateUserAssosAsStaff(discordId: string, assos: string[]): Observable<User> { 
    return this.http.patch<User>(`${this.apiURL}/users/${discordId}/assosAsStaff`, assos);
  }

  public updateUserAssosAsVisitor(discordId: string, assos: string[]): Observable<User> { 
    return this.http.patch<User>(`${this.apiURL}/users/${discordId}/assosAsVisitor`, assos);
  }

  public kickUser(discordId: string, comment: string): Observable<{message: string}> { 
    return this.http.post<{message: string}>(`${this.apiURL}/users/${discordId}/kick`, ({comment}));
  }

  public banUser(discordId: string, comment: string): Observable<{message: string}> { 
    return this.http.post<{message: string}>(`${this.apiURL}/users/${discordId}/ban`, ({comment}));
  }

  public staffUserStatus(discordId: string, assoId: string, status: string): Observable<{newStatus: "pending" | "accepted" | "refused"}> { 
    return this.http.patch<{newStatus: "pending" | "accepted" | "refused"}>(`${this.apiURL}/assos/${assoId}/staffs/${discordId}`, ({status}));
  }

  public visitorUserStatus(discordId: string, assoId: string, status: string): Observable<{newStatus: "pending" | "accepted" | "refused"}> { 
    return this.http.patch<{newStatus: "pending" | "accepted" | "refused"}>(`${this.apiURL}/assos/${assoId}/visitors/${discordId}`, ({status}));
  }

  public banUserFromAsso(discordId: string, assoId: string, isBan: boolean): Observable<void> { 
    return this.http.patch<void>(`${this.apiURL}/assos/${assoId}/visitors/${discordId}/ban`, {isBan});
  }

  public getAssoChannels(assoId: string): Observable<Channel[]> { 
    return this.http.get<Channel[]>(`${this.apiURL}/assos/${assoId}/channels`);
  }

  public deleteChannel(assoId: string, channelId: string): Observable<void> { 
    return this.http.delete<void>(`${this.apiURL}/assos/${assoId}/channels/${channelId}`);
  }

  public updateChannel(assoId: string, channelId: string, infos: Object): Observable<Channel> { 
    return this.http.patch<Channel>(`${this.apiURL}/assos/${assoId}/channels/${channelId}`, infos);
  }

  public createChannel(assoId: string, infos: Object): Observable<Channel> {
    return this.http.post<Channel>(`${this.apiURL}/assos/${assoId}/channels`, infos);
  }
}
