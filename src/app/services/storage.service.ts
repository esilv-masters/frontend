import { Injectable } from '@angular/core';
import { Asso } from '../models/asso';
import { User } from '../models/user';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  assos: Asso[];
  users: User[];

  constructor(
    private http: HttpService,
  ) { }

  getAssoByName(name: string): Asso | undefined {
    return this.assos?.find(asso => asso.name === name);
  }

  async getAssoNameByUrl(assoUrl: string): Promise<string | undefined> {
    const assoNames = await (await this.storeAssos()).map(a => a.name);
    const assoUrls = await this.fetchAssosUrl();
    return assoNames[assoUrls.indexOf(assoUrl)];
  }

  getUserByUsername(name: string): User | undefined {
    return this.users?.find(user => user.username === name);
  }

  getUserByDiscordId(id: string): User | undefined {
    return this.users?.find(user => user.discordId === id);
  }

  async storeAssos(): Promise<Asso[]> {
    if (this.assos === undefined) {
      const assos = await this.http.getAssos().toPromise();
      this.assos = assos;
    }
    return this.assos;
  }

  async fetchAssosUrl(): Promise<string[]> {
    const assos = await this.storeAssos();
    return assos.map(a => a.name.replace(/\s+/g, '-').toLowerCase());
  }
}
