import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { AuthService } from '@auth0/auth0-angular';
import { HttpService } from './http.service';
import { Observable, of } from 'rxjs';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  isUserLoaded = false;
  currUser: User;
  discordId: string;

  constructor(
    public auth: AuthService,
    private http: HttpService,
    private store: StorageService,
  ) { }

  initUser(): void {
    this.auth.user$.subscribe((user: any) => {
      this.discordId = user.sub.split('|')[2];

      this.http.getUserByDiscordId(this.discordId).subscribe((data: User) => {
        if (data !== null) {
          this.currUser = data;
        } else {
          this.currUser = {
            discordId: this.discordId,
            username: user.nickname,
            avatarURL: user.picture,
            isMod: false,
            isSpMod: false,
            isAdmin: false,
            isReferent: false,
            assosAsStaff: [],
            assosAsVisitor: [],
          }
        }
        if (!this.currUser.avatarURL) {
          this.currUser.avatarURL = user.picture;
        }
        if (!this.currUser.username) {
          this.currUser.username = user.nickname;
        }
        this.isUserLoaded = true;
      });
    });
  }

  isCompleteProfile(): boolean {
    if (this.currUser?.firstName && this.currUser?.lastName && this.currUser?.school && this.currUser?.schoolYear) {
      return this.currUser?.firstName !== '' && this.currUser?.lastName !== '' && this.currUser?.school !== '';
    }
    return false;
  }

  isStaff() : boolean {
    if (this.isModo()) {
      return false;
    }
    // Check if user is Staff pending or accepted in at least one asso
    const userAssos = this.currUser.assosAsStaff.map(asso => asso.userStatus !== 'refused');
    return this.currUser.isReferent || userAssos.length > 0;
  }

  isModo(): boolean {
    return this.currUser.isMod || this.currUser.isSpMod || this.currUser.isAdmin;
  }

  isReferent(assoName: string) : boolean {
    if (!this.currUser.isReferent) {
      return false;
    } else {
      return this.store.getAssoByName(assoName)?.referentId === this.currUser.discordId;
    }
  }

  async getUsers(): Promise<User[]> {
    if (this.store.users === undefined) {
      const users = await this.http.getUsers().toPromise();
      this.store.users = users;
      return users;
    } else {
      return this.store.users;
    }
  }
}
