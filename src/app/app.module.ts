import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations"
import { environment as env } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormInfosComponent } from './form-infos/form-infos.component';
import { HomeComponent } from './home/home.component';
import { AuthModule, AuthHttpInterceptor } from '@auth0/auth0-angular';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ModeratorComponent } from './moderator/moderator.component';
import { StaffComponent } from './staff/staff.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatTableModule} from '@angular/material/table';
import { AssoCardComponent } from './staff/asso-card/asso-card.component';
import { ReferentComponent } from './referent/referent.component';
import {MatTabsModule} from '@angular/material/tabs';
import { UserListComponent } from './staff/user-list/user-list.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatTooltipModule} from '@angular/material/tooltip';
import { DialogUserListComponent } from './staff/user-list/dialog/dialog-user-list.component';
import {MatDialogModule } from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ChannelListComponent } from './referent/channel-list/channel-list.component';
import { ChannelDialogComponent } from './referent/channel-dialog/channel-dialog.component';
import { ChannelFormComponent } from './referent/channel-form/channel-form.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import { CreateChannelDialogComponent } from './referent/create-channel-dialog/create-channel-dialog.component';
import {MatRadioModule} from '@angular/material/radio';
import { ManageDialogComponent } from './staff/user-list/manage-dialog/manage-dialog.component';
import {MatCheckboxModule} from '@angular/material/checkbox';



@NgModule({
  declarations: [
    AppComponent,
    FormInfosComponent,
    HomeComponent,
    TopBarComponent,
    ModeratorComponent,
    StaffComponent,
    AssoCardComponent,
    ReferentComponent,
    UserListComponent,
    DialogUserListComponent,
    ChannelListComponent,
    ChannelDialogComponent,
    ChannelFormComponent,
    CreateChannelDialogComponent,
    ManageDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    AuthModule.forRoot({
      domain: "ominga.eu.auth0.com",
      clientId: "joSK617Yx2RNCTBWM2zfAiPUR85fPF5v",
      redirectUri: "https://app.forum-associatif-numerique.fr",
      audience: "https://167.172.182.102/api",
      httpInterceptor: {
        allowedList: [{
          uri: "https://api.forum-associatif-numerique.fr/api/*"
        }],
      },
    }),


    AppRoutingModule,

    MatChipsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    HttpClientModule,
    MatTableModule,
    MatTabsModule,
    MatPaginatorModule,
    MatSortModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatRadioModule,
    MatCheckboxModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
