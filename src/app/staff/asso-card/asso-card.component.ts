import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Asso } from 'src/app/models/asso';
import { User } from 'src/app/models/user';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-asso-card',
  templateUrl: './asso-card.component.html',
  styleUrls: ['./asso-card.component.scss']
})
export class AssoCardComponent implements OnInit, OnChanges {

  @Input()
  asso: Asso;

  @Input()
  users: User[];

  assoStaff: string[];
  displayedStaffs: string;

  constructor(
    private userService: UserService,
    private store: StorageService,
    private router: Router,
  ) { }

  ngOnChanges(): void {
    if (this.users) {
      this.assoStaff = this.users.filter(user => user.assosAsStaff.filter(a => a.userStatus === 'accepted').map(a => a.assoName).includes(this.asso.name)).map(u => u.username);
      if (this.assoStaff.length === 0) {
        this.displayedStaffs = 'Aucun';
      } else {
        this.displayedStaffs = this.assoStaff.join(', ');
      }
    }
  }

  ngOnInit(): void {

  }

  getColor(type: string): string {
    if (this.getStatus() === 'admin') {
      return 'orange-' + type;
    }
    if (this.getStatus() === 'en attente') {
      return 'blue-' + type;
    } else if (this.getStatus() === 'accepté') {
      return 'purple-' + type;
    } else {
      return 'red-' + type;
    }
  }

  getStatus(): string {
    const userStatus = this.userService.currUser.assosAsStaff.find(a => a.assoName === this.asso.name)?.userStatus;
    if (this.isModoOrReferent()) {
      return 'admin';
    }
    if (userStatus === 'pending') {
      return 'en attente';
    } else if (userStatus === 'accepted') {
      return 'accepté';
    } else {
      return 'refusé';
    }
  }

  isModoOrReferent(): boolean {
    return this.userService.isModo() || this.userService.isReferent(this.asso.name);
  }

  navigateToAsso(assoName: string): void {
    this.router.navigate([assoName.replace(/\s+/g, '-').toLowerCase()])
  }

}
