import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Asso } from '../models/asso';
import { User } from '../models/user';
import { HttpService } from '../services/http.service';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {

  allAssos: Asso[];
  userAssos: Asso[];
  users: User[];

  selectable = true;
  removable = true;
  filteredAssosAsStaff: Observable<string[]> | undefined;
  assosAsStaff: string[] = [];
  allAssosName: string[];
  assoAsStaff = new FormControl(null);

  @ViewChild('assoStaffInput') assoStaffInput!: ElementRef<HTMLInputElement>;

  constructor(
    private userService: UserService,
    private http: HttpService,
    private store: StorageService,
    private router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    if (!this.userService.isCompleteProfile()) {
      this.router.navigate(['/infos']);
    }
    if (!this.userService.isStaff()) { 
      // redirect to home
      this.router.navigate(['']);
    }

    this.storeUsers();
    
    await this.fetchAssos();

    this.filteredAssosAsStaff = this.assoAsStaff?.valueChanges.pipe(
      startWith(null),
      map((asso: string | null) => asso ? this._filter(asso) : this.allAssosName?.slice())
    );
  }

  async storeUsers(): Promise<void> {
    this.users = await this.userService.getUsers();
  }

  async fetchAssos(): Promise<void> {
    const data = await this.store.storeAssos();

    this.allAssos = data;

    // remove assos that he's already staffing
    const userAssosAsStaff = this.userService.currUser.assosAsStaff.map(a => a.assoName); // String array
    this.allAssosName = data.map(asso => asso.name).filter(asso => !userAssosAsStaff.includes(asso));

    this.userAssos = this.allAssos.filter(a => userAssosAsStaff.includes(a.name)).sort((a, b) => this.compare(b, a, this.userService.currUser)); // Object array

  }

  addStaff(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.assosAsStaff.push(value);
    }
    event.chipInput!.clear();
    this.assoAsStaff?.setValue(null);
  }

  removeStaff(fruit: string): void {
    const index = this.assosAsStaff.indexOf(fruit);
    if (index >= 0) {
      this.assosAsStaff.splice(index, 1);
    }
  }

  selectedStaff(event: MatAutocompleteSelectedEvent): void {
    this.assosAsStaff.push(event.option.viewValue);
    this.assoStaffInput.nativeElement.value = '';
    this.assoAsStaff?.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();  
    return this.allAssosName.filter(asso => asso.toLowerCase().includes(filterValue));
  }

  post(): void {
    if (this.assosAsStaff.length > 0) {
      this.http.updateUserAssosAsStaff(this.userService.currUser.discordId, this.assosAsStaff).subscribe((res: User) => {
        this.userService.currUser = res;
        this.assosAsStaff = [];
        this.fetchAssos();
      });
    }
  }

  compare(a1: Asso, a2: Asso, user: User): number {
    const userAssosBis = user.assosAsStaff;
    const a1Status = userAssosBis.find(a => a.assoName === a1.name)?.userStatus;
    const a2Status = userAssosBis.find(a => a.assoName === a2.name)?.userStatus;

    if (a1.referentId === user.discordId) {
      return 1;
    } else if (a2.referentId === user.discordId) {
      return -1;
    }
    if (a1Status === 'accepted') {
      return 1;
    } else if (a2Status === 'accepted') {
      return -1;
    } else if (a1Status === 'pending') {
      return 1;
    } else if (a2Status === 'pending') {
      return -1;
    } else if (a1Status === 'refused') {
      return 1;
    } else {
      return -1;
    }
  }

  navigateToDiscord(): void {
    window.open('https://discord.gg/X77ztWs', '_blank');
  }

}
