import { AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserData } from '../user-list.component';

interface AssoList {
  userStatus: 'pending' | 'accepted' | 'refused';
  userRole: 'referent' | 'staff' | 'visiteur';
  assoName: string;
  assoEmoji?: string;
}

@Component({
  selector: 'app-manage-dialog',
  templateUrl: './manage-dialog.component.html',
  styleUrls: ['./manage-dialog.component.scss']
})
export class ManageDialogComponent implements OnInit, AfterViewInit {

  userForm = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    ecole: new FormControl('', Validators.required),
    annee: new FormControl(null, Validators.required),
  });

  userAssoForm = new FormGroup({
    name: new FormControl(''),
  });
  isStaff = false;
  assos: string[];
  filteredAssos: Observable<string[]> | undefined;

  canValidate = true;

  addAssoPanel = false;
  tablePanel = false;


  assoList: AssoList[] = [];

  dataSource: MatTableDataSource<AssoList>;
  displayedColumns: string[] = ['userStatus', 'userRole', 'assoName', 'actions'];

  actions: string[] = ['accept', 'deny', 'assoBan'];


  ecoles = ['EMLV', 'ESILV', 'IIM'];
  annees = [1, 2, 3, 4, 5];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {user: UserData},
    private store: StorageService,
    private http: HttpService,
    public dialogRef: MatDialogRef<ManageDialogComponent>
  ) { }

  ngAfterViewInit(): void {
    this.initSortAndPagine();
  }

  ngOnInit(): void {
    this.setForm();
    this.completeDatasource();
    this.dataSource = new MatTableDataSource(this.assoList);

    const datasourceAssos = this.dataSource.data.map(a => a.assoName);
    this.assos = this.store.assos.filter(a => !datasourceAssos.includes(a?.name)).map(a => a.name);

    this.filteredAssos = this.name?.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  setForm() {
    this.userForm.setValue({
      prenom: this.data.user.firstName ? this.data.user.firstName : '',
      nom: this.data.user.lastName ? this.data.user.lastName : '',
      ecole: this.data.user.school ? this.data.user.school : '',
      annee: this.data.user.schoolYear ? this.data.user.schoolYear : '',
    });
  }

  initSortAndPagine(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  completeDatasource(): void {
    this.data.user.assosAsStaff.forEach(a => {
      const asso = this.store.getAssoByName(a.assoName);
      this.assoList.push({
        userStatus: a.userStatus ? a.userStatus : 'pending',
        userRole: asso?.referentId === this.data.user.discordId ? 'referent' : 'staff',
        assoName: asso?.name ? asso.name : '', 
        assoEmoji: asso?.emoji ? asso.emoji : ''
      });
    });

    this.data.user.assosAsVisitor.forEach(a => {
      const asso = this.store.getAssoByName(a.assoName);
      if (!a.userIsBan) {
        this.assoList.push({
          userStatus: a.userStatus ? a.userStatus : 'pending',
          userRole: 'visiteur',
          assoName: asso?.name ? asso.name : '', 
          assoEmoji: asso?.emoji ? asso.emoji : ''
        });
      }
    });
  }

  onValidate(): void {
    if (this.userForm.valid) {
      this.canValidate = false;
      const infos = {
        firstName: this.prenom?.value,
        lastName: this.nom?.value,
        school: this.ecole?.value,
        schoolYear: this.annee?.value
      }
      this.http.updateUserInfos(this.data.user.discordId, infos).subscribe(res => {
        this.dialogRef.close(res);
      });
    } else {
      this.userForm.markAllAsTouched();
    }
  }

  userIsBan(asso: AssoList): boolean {
    const isBan = this.data.user.assosAsVisitor.find(a => a.assoName === asso.assoName)?.userIsBan;
    if (!isBan) {
      return false;
    } else {
      return isBan;
    }
  }

  assoBan(element: AssoList): void {
    const asso = this.store.getAssoByName(element.assoName);
    if (asso) {
      this.http.banUserFromAsso(this.data.user.discordId, asso._id, true).subscribe( () => {
        // delete row
        const index = this.dataSource.data.map(a => a.assoName).indexOf(element.assoName);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription();
        
        //@ts-ignore
        this.data.user.assosAsVisitor.find(a => a.assoName === element.assoName)?.userIsBan = true;
        
        this.assos.push(element.assoName);
      });
    }
  }

  accept(asso: AssoList): void {
    const assoId = this.store.getAssoByName(asso.assoName)?._id;
    if (assoId !== undefined) {
      if (asso.userRole === 'visiteur') {
        this.http.visitorUserStatus(this.data.user.discordId, assoId?.toString(), 'accepted').subscribe(res => {
          //@ts-ignore
          this.dataSource.data.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
          //@ts-ignore
          this.data.user?.assosAsVisitor.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
        });
      } else {
        this.http.staffUserStatus(this.data.user.discordId, assoId?.toString(), 'accepted').subscribe(res => {
          //@ts-ignore
          this.dataSource.data.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
          //@ts-ignore
          this.data.user?.assosAsStaff.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
        });
      }
    }
  }

  deny(asso: AssoList): void {
    const assoId = this.store.getAssoByName(asso.assoName)?._id;
    if (assoId !== undefined) {
      if (asso.userRole === 'visiteur') {
        this.http.visitorUserStatus(this.data.user.discordId, assoId?.toString(), 'refused').subscribe(res => {
          //@ts-ignore
          this.dataSource.data.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
          //@ts-ignore
          this.data.user?.assosAsVisitor.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
        });
      } else {
        this.http.staffUserStatus(this.data.user.discordId, assoId?.toString(), 'refused').subscribe(res => {
          //@ts-ignore
          this.dataSource.data.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
          //@ts-ignore
          this.data.user?.assosAsStaff.find(a => a.assoName === asso.assoName)?.userStatus = res.newStatus;
        });
      }
    }
  }

  isAcceptable(asso: AssoList): boolean {
    if (asso.userRole === 'visiteur') {
      return false;
    } else {
      if (this.data.user?.assosAsStaff.find(a => asso.assoName.includes(a.assoName))?.userStatus === 'accepted') {
        return false;
      }
    }
    return true;
  }

  isRefusable(asso: AssoList): boolean {
    if (asso.userRole === 'visiteur') {
      return false;
    } else {
      if (this.data.user?.assosAsStaff.find(a => asso.assoName.includes(a.assoName))?.userStatus === 'refused') {
        return false;
      }
    }
    return true;
  }

  addAsso(): void {
    if (this.name?.value !== '') {
      if (this.isStaff) {
        this.http.updateUserAssosAsStaff(this.data.user.discordId, [this.name?.value]).subscribe(res => {
          this.assos = this.assos.filter(a => a !== this.name?.value);
          this.dataSource.data.push({
            userStatus: 'pending',
            userRole: 'staff',
            assoName: this.name?.value,
            assoEmoji: this.store.getAssoByName(this.name?.value)?.emoji
          });
          this.dataSource._updateChangeSubscription();
          this.name?.patchValue('');
          this.data.user = res;
        });
      } else {
        this.http.updateUserAssosAsVisitor(this.data.user.discordId, [this.name?.value]).subscribe(res => {
          this.assos = this.assos.filter(a => a !== this.name?.value);
          this.dataSource.data.push({
            userStatus: 'accepted',
            userRole: 'visiteur',
            assoName: this.name?.value,
            assoEmoji: this.store.getAssoByName(this.name?.value)?.emoji
          });
          this.dataSource._updateChangeSubscription();
          this.name?.patchValue('');
          this.data.user = res;
        });
      }
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.assos.filter(a => a.toLowerCase().includes(filterValue));
  }

  get name() { return this.userAssoForm.get('name'); }
  get nom() { return this.userForm.get('nom'); }
  get prenom() { return this.userForm.get('prenom'); }
  get ecole() { return this.userForm.get('ecole'); }
  get annee() { return this.userForm.get('annee'); }

}
