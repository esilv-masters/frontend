import { Input, ViewChild, Component, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/user';
import { HttpService } from 'src/app/services/http.service';
import { UserService } from 'src/app/services/user.service';
import { DialogUserListComponent } from './dialog/dialog-user-list.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import { StorageService } from 'src/app/services/storage.service';
import { ManageDialogComponent } from './manage-dialog/manage-dialog.component';

export interface UserData extends User {
  role?: number;
  status?: number;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnChanges {

  @Input()
  allUsers: User[];

  @Input()
  listSize: number;

  @Input()
  actions: string[];

  @Input()
  assoName: string;

  @Input()
  page: 'staff' | 'visitor';

  @Input()
  userType: 'role' | 'status' |'';

  users: UserData[] = [];
  
  dataSource: MatTableDataSource<UserData>;
  displayedColumns: string[];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private userService: UserService,
    private http: HttpService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private store: StorageService,
    ) { 
    }

  ngOnChanges(): void {

    if (this.allUsers !== undefined) {
      this.users = this.allUsers;
      if (this.userType === 'role') {
        this.fetchUsersRole();
      } else  if (this.userType === 'status'){
        this.fetchUsersStatus();
      } else {
        this.displayedColumns = ['username', 'firstName', 'lastName', 'school', 'schoolYear', 'actions'];
      }
      this.dataSource = new MatTableDataSource(this.users);
      
      this.initSortAndPagine();
    }
  }

  initSortAndPagine(): void {
    this.dataSource.paginator = this.paginator;

    this.dataSource.sort = this.sort;

    const sortState: Sort = {active: this.userType, direction: 'desc'};
    this.sort.active = sortState.active;
    this.sort.direction = sortState.direction;
    this.sort.sortChange.emit(sortState);
  }

  fetchUsersRole(): void {
    this.displayedColumns = ['role', 'username', 'firstName', 'lastName', 'school', 'schoolYear', 'actions'];
    this.users.forEach(user => {
      if (user.isAdmin) {
        user.role = 5;
      } else if (user.isSpMod) {
        user.role = 4;
      } else if (user.isMod) {
        user.role = 3;
      } else if (user.isReferent) {
        user.role = 2;
      } else if (this.userService.isStaff()) {
        user.role = 1;
      } else {
        user.role = 0;
      }
    });
  }

  fetchUsersStatus(): void {
    this.displayedColumns = ['status', 'username', 'firstName', 'lastName', 'school', 'schoolYear', 'actions'];
    this.users.forEach(user => {
      if (this.page === 'staff') {
        user.status = this.fetchStatus(user.assosAsStaff.find(a => a.assoName === this.assoName)?.userStatus)
      } else {
        user.status = this.fetchStatus(user.assosAsVisitor.find(a => a.assoName === this.assoName)?.userStatus)
      }
    });
  }

  fetchStatus(status: string | undefined): number {
    if (status === 'accepted') {
      return 2;
    } else if (status === 'pending') {
      return 1;
    } else {
      return 0;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  kick(id: string): void {
    const dialogRef = this.dialog.open(DialogUserListComponent, {
      data: {
        typeBan: false,
        username: this.users.find(u => u.discordId === id)?.username,
        comment: '',
        result: false,
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data?.result) {
        this.http.kickUser(id, data.comment).subscribe(res => {
          this.deleteRow(id);
          this.openSnackBar(res.message);
        });
      }
    });
  }

  ban(id: string): void {
    const dialogRef = this.dialog.open(DialogUserListComponent, {
      data: {
        typeBan: true,
        username: this.users.find(u => u.discordId === id)?.username,
        comment: '',
        result: false,
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data?.result) {
        this.http.banUser(id, data.comment).subscribe(res => {
          this.deleteRow(id);
          this.openSnackBar(res.message);
        });
      }
    });
  }

  assoBan(id: string): void {
    const asso = this.store.getAssoByName(this.assoName);
    if (asso) {
      this.http.banUserFromAsso(id, asso._id, !this.userIsBan(id)).subscribe( () => {
        const user = this.dataSource.data.find(u => u.discordId === id);
        if (user) {
          //@ts-ignore
          user.assosAsVisitor.find(a => a.assoName === this.assoName)?.userIsBan = !this.userIsBan(id);
        }
      });
    }
  }

  userIsBan(id: string): boolean {
    const asso = this.store.getAssoByName(this.assoName);
    const user = this.dataSource.data.find(u => u.discordId === id)
    if (asso && user) {
      const isBan = user.assosAsVisitor.find(a => a.assoName === this.assoName)?.userIsBan;
      if (isBan) {
        return true;
      }
    }
    return false;
  }

  deleteRow(id: string): void {
    const index = this.dataSource.data.map(u => u.discordId).indexOf(id);
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }

  openSnackBar(message: string): void {
    this._snackBar.open(message, 'fermer', {
      duration: 5000
    });
  }

  accept(id: string): void {
    const assoId = this.store.getAssoByName(this.assoName)?._id;
    if (assoId !== undefined) {
      if (this.page === 'staff') {
        this.http.staffUserStatus(id, assoId?.toString(), 'accepted').subscribe(newStatus => {
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.assosAsStaff.find(a => a.assoName === this.assoName)?.userStatus = newStatus.newStatus;
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.status = this.fetchStatus(newStatus.newStatus);
        });
      } else {
        this.http.visitorUserStatus(id, assoId?.toString(), 'accepted').subscribe(newStatus => {
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.assosAsVisitor.find(a => a.assoName === this.assoName)?.userStatus = newStatus.newStatus;
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.status = this.fetchStatus(newStatus.newStatus);
        });
      }
    }
  }

  deny(id: string): void {
    const assoId = this.store.getAssoByName(this.assoName)?._id;
    if (assoId !== undefined) {
      if (this.page === 'staff') {
        this.http.staffUserStatus(id, assoId?.toString(), 'refused').subscribe(newStatus => {
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.assosAsStaff.find(a => a.assoName === this.assoName)?.userStatus = newStatus.newStatus;
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.status = this.fetchStatus(newStatus.newStatus);
        });
      } else {
        this.http.visitorUserStatus(id, assoId?.toString(), 'refused').subscribe(newStatus => {
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.assosAsVisitor.find(a => a.assoName === this.assoName)?.userStatus = newStatus.newStatus;
          //@ts-ignore
          this.dataSource.data.find(u => u.discordId === id)?.status = this.fetchStatus(newStatus.newStatus);
        });
      }
    }
  }

  isAcceptable(id: string): boolean {
    const user = this.users.find(u => u.discordId === id);
    if (this.page === 'staff') {
      if (user?.assosAsStaff.find(a => a.assoName === this.assoName)?.userStatus === 'accepted') {
        return false;
      }
    } else {
      if (user?.assosAsVisitor.find(a => a.assoName === this.assoName)?.userStatus === 'accepted') {
        return false;
      }
    }
    return true;
  }

  isRefusable(id: string): boolean {
    const user = this.users.find(u => u.discordId === id);
    if (this.page === 'staff') {
      if (user?.assosAsStaff.find(a => a.assoName === this.assoName)?.userStatus === 'refused') {
        return false;
      }
    } else {
      if (user?.assosAsVisitor.find(a => a.assoName === this.assoName)?.userStatus === 'refused') {
        return false;
      }
    }
    return true;
  }

  manage(id: string): void {
    const dialogRef = this.dialog.open(ManageDialogComponent, {
      data: {
        user: this.users.find(u => u.discordId === id),
      }
    });

    dialogRef.afterClosed().subscribe((data: User) => {
      if (data) {
        const user = this.dataSource.data.find(u => u.discordId === data.discordId);
        if (user) {
          let index = this.dataSource.data.indexOf(user);
          const userDS = this.dataSource.data[index];
          this.dataSource.data[index] = {...data, role: userDS.role, status: userDS.status};
          this.dataSource._updateChangeSubscription();
        }
      }
    });
  }

}
