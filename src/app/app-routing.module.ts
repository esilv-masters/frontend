import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormInfosComponent } from './form-infos/form-infos.component';
import { AuthGuard } from '@auth0/auth0-angular';
import { ModeratorComponent } from './moderator/moderator.component';
import { StaffComponent } from './staff/staff.component';
import { ReferentComponent } from './referent/referent.component';

const routes: Routes = [
  { 
    path: 'infos', 
    component: FormInfosComponent,
    canActivate: [AuthGuard],
  },
  { 
    path: 'staff', 
    component: StaffComponent,
    canActivate: [AuthGuard],
  },
  { 
    path: 'moderator', 
    component: ModeratorComponent,
    canActivate: [AuthGuard],
  },
  { 
    path: ':assoName', 
    component: ReferentComponent,
    canActivate: [AuthGuard],
  },
  { 
    path: '', 
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  { 
    path: '**', 
    redirectTo: '' 
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
