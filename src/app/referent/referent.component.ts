import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Asso } from '../models/asso';
import { Channel } from '../models/channel';
import { User } from '../models/user';
import { HttpService } from '../services/http.service';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateChannelDialogComponent } from './create-channel-dialog/create-channel-dialog.component';

@Component({
  selector: 'app-referent',
  templateUrl: './referent.component.html',
  styleUrls: ['./referent.component.scss']
})
export class ReferentComponent implements OnInit {

  staffs: User[];
  visitors: User[];

  assoName: string = '';

  asso: Asso | undefined;

  channels: Channel[] = [];

  constructor(
    private userService: UserService,
    private http: HttpService,
    private store: StorageService,
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (!this.userService.isCompleteProfile()) {
      this.router.navigate(['/infos']);
    }

    this.route.params.subscribe(async params => {
      const assoUrl = params.assoName;
      const assoNameTMP = await this.store.getAssoNameByUrl(assoUrl);

      if (assoNameTMP === undefined) {
        this.router.navigate(['']);
      } else if (!(this.userService.isModo() || this.userService.isReferent(assoNameTMP))) { 
        this.router.navigate(['']);
      } else {
        this.assoName = assoNameTMP;
      }

      this.storeUsers();

      this.asso = this.store.getAssoByName(this.assoName);

      this.fetchChannels();

    });
  }

  async storeUsers(): Promise<void> {
    const users = await this.userService.getUsers();

    this.staffs = users.filter(u => u.assosAsStaff.map(a => a.assoName).includes(this.assoName));
    this.visitors = users.filter(u => u.assosAsVisitor.map(a => a.assoName).includes(this.assoName));
  }

  async fetchChannels(): Promise<void> {
    if (this.asso) {
      this.channels = await this.http.getAssoChannels(this.asso._id).toPromise();
    }
  }

  addNewChannel(): void {
    const dialogRef = this.dialog.open(CreateChannelDialogComponent, {
      width: '400px',
      data: {
        asso: this.asso,
        chanLen: this.channels.length
      }
    });

    dialogRef.afterClosed().subscribe((res: any) => {
      this.fetchChannels();
    });
  }

  back(): void {
    this.router.navigate(['']);
  }

}
