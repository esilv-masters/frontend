import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Asso } from 'src/app/models/asso';
import { Channel, PermId, Permission } from 'src/app/models/channel';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-channel-dialog',
  templateUrl: './create-channel-dialog.component.html',
  styleUrls: ['./create-channel-dialog.component.scss']
})
export class CreateChannelDialogComponent implements OnInit {

  newChannelFrom = new FormGroup({
    channel: new FormControl([], Validators.maxLength(5-this.data.chanLen)),
    name: new FormControl(''),
    vRead: new FormControl(false),
    vWrite: new FormControl(false),
    vConnect: new FormControl(false),
    vSpeak: new FormControl(false),
  });

  canVal = true;

  channelList: string[] = ['🔺┃informations', '💬┃discussion', '⛔┃staff', '❓┃questions', '🔊┃Vocal', '🛂┃Entretien', '🌐┃Conférence'];

  custom = false;

  isVoice = false;

  emojis = {
    text: '💬',
    voice: '🔊'
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {asso: Asso, chanLen: number},
    private http: HttpService,
    public dialogRef: MatDialogRef<CreateChannelDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  get name() { return this.newChannelFrom.get('name'); }
  get channel() { return this.newChannelFrom.get('channel'); }

  closeDialog(data?: any): void {
    this.dialogRef.close(data);
  }

  async onValidate(): Promise<void> {
    if (this.channel && this.newChannelFrom.valid) {
      this.canVal = false;
      for (let c of this.channel.value) {
        if (c.includes('informations')) {
          const chan = {
            name: '🔺┃informations',
            type: 'text',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view],
                deny: [PermId.write]
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else if (c.includes('discussion')) {
          const chan = {
            name: '💬┃discussion',
            type: 'text',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view, PermId.write],
                deny: []
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else if (c.includes('staff')) {
          const chan = {
            name: '⛔┃staff',
            type: 'text',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [],
                deny: [PermId.view, PermId.write]
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else if (c.includes('questions')) {
          const chan = {
            name: '❓┃questions',
            type: 'text',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view, PermId.write],
                deny: []
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else if (c.includes('Entretien')) {
          const chan = {
            name: '🛂┃Entretien',
            type: 'voice',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view, PermId.speak],
                deny: [PermId.connect]
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else if (c.includes('Conférence')) {
          const chan = {
            name: '🌐┃Conférence',
            type: 'voice',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view, PermId.connect],
                deny: [PermId.speak]
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        } else {
          const chan = {
            name: '🔊┃Vocal',
            type: 'voice',
            permissionOverwrites: [
              {
                id: "visitorRoleId",
                allow: [PermId.view, PermId.connect, PermId.speak],
                deny: []
              },
            ]
          };
          await this.http.createChannel(this.data.asso._id, chan).toPromise();
        }
      }
      if (this.name?.value !== '' && this.canCreateCustomChan()) {
        const chan: {
          name: string,
          type: 'voice' | 'text',
          permissionOverwrites: Permission[];
        } = {
          name: `${this.emojis[this.isVoice ? 'voice' : 'text']}┃${this.name?.value}`,
          type: this.isVoice ? 'voice' : 'text',
          permissionOverwrites: [
            {
              id: "visitorRoleId",
              allow: [],
              deny: []
            },
          ]
        };
        if (this.isVoice) {
          this.newChannelFrom.get('vConnect')?.value ? chan.permissionOverwrites[0].allow.push(PermId.connect) : chan.permissionOverwrites[0].deny.push(PermId.connect);
          this.newChannelFrom.get('vSpeak')?.value ? chan.permissionOverwrites[0].allow.push(PermId.speak) : chan.permissionOverwrites[0].deny.push(PermId.speak);
        } else {
          this.newChannelFrom.get('vWrite')?.value ? chan.permissionOverwrites[0].allow.push(PermId.write) : chan.permissionOverwrites[0].deny.push(PermId.write);
        }
        this.newChannelFrom.get('vRead')?.value ? chan.permissionOverwrites[0].allow.push(PermId.view) : chan.permissionOverwrites[0].deny.push(PermId.view);
        await this.http.createChannel(this.data.asso._id, chan).toPromise();
      }
      this.closeDialog();
    }
  }

  canCreateCustomChan(): boolean {
    return this.data.chanLen + this.channel?.value.length < 5;
  }

}
