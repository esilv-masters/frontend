import { EventEmitter, Input } from '@angular/core';
import { Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Asso } from 'src/app/models/asso';
import { Channel, PermId, Permission } from 'src/app/models/channel';
import { HttpService } from 'src/app/services/http.service';
import { ChannelDialogComponent } from '../channel-dialog/channel-dialog.component';

@Component({
  selector: 'app-channel-form',
  templateUrl: './channel-form.component.html',
  styleUrls: ['./channel-form.component.scss']
})
export class ChannelFormComponent implements OnInit {

  channelForm = new FormGroup({
    name: new FormControl('', Validators.required),
    vRead: new FormControl(false, Validators.required),
    vWrite: new FormControl(false, Validators.required),
    vConnect: new FormControl(false, Validators.required),
    vSpeak: new FormControl(false, Validators.required),
  });


  constructor(
    private http: HttpService,
    public dialogRef: MatDialogRef<ChannelDialogComponent>
  ) { }

  @Input()
  channelToUpdate: Channel;

  @Input()
  channelType: 'text' | 'voice';

  @Input()
  asso: Asso;

  ngOnInit(): void {
    if (this.channelToUpdate) {
      this.channelForm.setValue({
        name: this.channelToUpdate.name,
        ...this.transformPermsToBool()
      });
    }

  }

  get name() { return this.channelForm.get('name'); }

  onValidate(): void {
    if (this.channelForm.valid) {
      if (this.channelToUpdate) {
        let updatedChannel = {...this.channelToUpdate};
        updatedChannel.name = this.name?.value;
        updatedChannel.permissionOverwrites[0].allow = [];
        updatedChannel.permissionOverwrites[0].deny = [];
        this.channelForm.get('vRead')?.value ? updatedChannel.permissionOverwrites[0].allow.push(PermId.view) : updatedChannel.permissionOverwrites[0].deny.push(PermId.view);
        this.channelForm.get('vWrite')?.value ? updatedChannel.permissionOverwrites[0].allow.push(PermId.write) : updatedChannel.permissionOverwrites[0].deny.push(PermId.write);
        this.channelForm.get('vConnect')?.value ? updatedChannel.permissionOverwrites[0].allow.push(PermId.connect) : updatedChannel.permissionOverwrites[0].deny.push(PermId.connect);
        this.channelForm.get('vSpeak')?.value ? updatedChannel.permissionOverwrites[0].allow.push(PermId.speak) : updatedChannel.permissionOverwrites[0].deny.push(PermId.speak);

        this.http.updateChannel(this.asso._id, this.channelToUpdate.id, updatedChannel).subscribe(newChannel => {
          this.closeDialog(newChannel);
        })
      }
    }
  }

  transformPermsToBool(): {
    vRead: boolean,
    vWrite: boolean,
    vConnect: boolean,
    vSpeak: boolean,
  } {
    let res = {
      vRead: false,
      vWrite: false,
      vConnect: false,
      vSpeak: false,
    };
    res.vRead = this.channelToUpdate.permissionOverwrites[0].allow.includes(PermId.view);
    res.vWrite = this.channelToUpdate.permissionOverwrites[0].allow.includes(PermId.write);
    res.vConnect = this.channelToUpdate.permissionOverwrites[0].allow.includes(PermId.connect);
    res.vSpeak = this.channelToUpdate.permissionOverwrites[0].allow.includes(PermId.speak);
    return res;
  }

  closeDialog(data?: Channel): void {
    this.dialogRef.close(data);
  }

}
