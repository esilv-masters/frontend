import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Asso } from 'src/app/models/asso';
import { Channel } from 'src/app/models/channel';

@Component({
  selector: 'app-channel-dialog',
  templateUrl: './channel-dialog.component.html',
  styleUrls: ['./channel-dialog.component.scss']
})
export class ChannelDialogComponent implements OnInit {

  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {asso: Asso, channel: Channel}
  ) { }

  ngOnInit(): void {
  }

  validate(): void {
    
  }

}
