import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Asso } from 'src/app/models/asso';
import { Channel, Permission } from 'src/app/models/channel';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { ChannelDialogComponent } from '../channel-dialog/channel-dialog.component';

@Component({
  selector: 'app-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss']
})
export class ChannelListComponent implements OnInit, OnChanges {

  permIdTranslation =  {
    VIEW_CHANNEL: "Voir",
    SEND_MESSAGES: "Écrire",
    CONNECT: "Se connecter",
    SPEAK: "Parler",
    MOVE_MEMBERS: "Déplacer des membres"
  }

  @ViewChild(MatSort, {static: false}) sort: MatSort;

  @Input()
  channels: Channel[] = [];

  @Input()
  asso: Asso | undefined;
  
  dataSource: MatTableDataSource<Channel>;
  displayedColumns: string[] = ['type', 'name', 'visitorPerms', 'actions'];

  constructor(
    private userService: UserService,
    private http: HttpService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private store: StorageService,
  ) { }


  ngOnChanges(): void {
    if (this.channels !== undefined) {
      this.dataSource = new MatTableDataSource(this.channels);
      
      this.initSort();
    }
  }

  ngOnInit(): void {
  }

  initSort(): void {
    if (this.sort) {
      this.dataSource.sort = this.sort;
  
      const sortState: Sort = {active: 'type', direction: 'asc'};
      this.sort.active = sortState.active;
      this.sort.direction = sortState.direction;
      this.sort.sortChange.emit(sortState);
    }
  }

  getVisitorPerm(allPerms: Permission[]): string {
    let res = 'Aucune';
    allPerms.forEach(perm => {
      if (perm.id === 'visitorRoleId' && perm.allow.length > 0) {
        const perms: string[] = [];
        perm.allow.forEach(p => perms.push(this.permIdTranslation[p]));
        res = perms.join(', ');
      }
    });
    return res;
  }

  deleteChannel(channelId: string): void {
    if (this.asso) {
      this.http.deleteChannel(this.asso._id, channelId).subscribe(() => {
        this.deleteRow(channelId);
        this.openSnackBar("Suppression effectuée avec succès !");
      });
    }
  }

  deleteRow(id: string): void {
    const index = this.dataSource.data.map(c => c.id).indexOf(id);
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }

  openSnackBar(message: string): void {
    this._snackBar.open(message, 'fermer', {
      duration: 5000
    });
  }

  updateChannel(channelId: string): void {
    const dialogRef = this.dialog.open(ChannelDialogComponent, {
      width: '400px',
      data: {
        asso: this.asso,
        channel: this.findChannelById(channelId)
      }
    });

    dialogRef.afterClosed().subscribe((res: Channel) => {
      if (res) {
        this.openSnackBar(`${res.name} a été modifié !`);
      }
    });
  }

  findChannelById(id: string): Channel |undefined {
    return this.channels.find(c => c.id === id);
  }

}
