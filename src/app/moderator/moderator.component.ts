import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Asso } from '../models/asso';
import { User } from '../models/user';
import { HttpService } from '../services/http.service';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-moderator',
  templateUrl: './moderator.component.html',
  styleUrls: ['./moderator.component.scss']
})
export class ModeratorComponent implements OnInit {

  users: User[];
  allAssos: Asso[];
  filteredAssos: Asso[];
  
  searchForm = new FormGroup({
    searchAsso: new FormControl(''),
  });
  

  constructor(
    private userService: UserService,
    private http: HttpService,
    private store: StorageService,
    private router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    if (!this.userService.isCompleteProfile()) {
      this.router.navigate(['/infos']);
    }
    if (!this.userService.isModo()) { 
      // redirect to home
      this.router.navigate(['']);
    }

    this.storeUsers();

    this.filteredAssos = await this.store.storeAssos();
    this.allAssos = await this.store.storeAssos();

    this.searchForm.get('searchAsso')?.valueChanges.subscribe(value => {
      this.filteredAssos = this.allAssos.filter(asso => asso.name.toLowerCase().includes(value.toLowerCase()));
    });
  }

  async storeUsers(): Promise<void> {
    this.users = await this.userService.getUsers();
  }

}
