// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  auth: {
    domain: "ominga.eu.auth0.com",
    clientId: "joSK617Yx2RNCTBWM2zfAiPUR85fPF5v",
    redirectUri: window.location.origin,
    audience: "http://localhost:8080/api"
  },
  dev: {
    serverUrl: "http://localhost:8080"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
