export const environment = {
    production: true,
    auth: {
      domain: "ominga.eu.auth0.com",
      clientId: "joSK617Yx2RNCTBWM2zfAiPUR85fPF5v",
      redirectUri: window.location.origin,
      audience: "https://10.114.0.4:8080/api"
    },
    prod: {
      serverUrl: "https://10.114.0.4:8080"
    }
  };